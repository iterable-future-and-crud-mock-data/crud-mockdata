import 'package:dog_app/dog_app_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Dog App',
    home: DogApp(),
  ));
}
