import 'package:dog_app/dog.dart';
import 'package:dog_app/dog_form_widget.dart';
import 'package:dog_app/dog_service.dart';
import 'package:flutter/material.dart';

class DogApp extends StatefulWidget {
  DogApp({Key? key}) : super(key: key);

  @override
  _DogAppState createState() => _DogAppState();
}

class _DogAppState extends State<DogApp> {
  late Future<List<Dog>> _dogs;
  @override
  void initState() {
    super.initState();
    _dogs = getDog();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dog App'),
      ),
      body: FutureBuilder(
        future: _dogs,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<Dog> dogs = snapshot.data as List<Dog>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var dog = dogs.elementAt(index);
              return ListTile(
                title: Text(dog.name),
                subtitle: Text('id: ${dog.id} age: ${dog.age}'),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    await delDog(dog);
                    setState(() {
                      _dogs = getDog();
                    });
                  },
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DogForm(dog: dog)));
                  setState(() {
                    _dogs = getDog();
                  });
                },
              );
            },
            itemCount: dogs.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Dog newdog = Dog(id: -1, name: '', age: 0);
          await Navigator.push(context,
              MaterialPageRoute(builder: (context) => DogForm(dog: newdog)));
          setState(() {
            _dogs = getDog();
          });
        },
      ),
    );
  }
}
